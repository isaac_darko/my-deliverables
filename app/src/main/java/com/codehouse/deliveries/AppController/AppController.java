package com.codehouse.deliveries.AppController;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class AppController extends Application {


	private android.support.multidex.MultiDexApplication multi;
	public static final String TAG = AppController.class
			.getSimpleName();


	private static AppController mInstance;
	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		multi = new android.support.multidex.MultiDexApplication();
		Realm.init(getApplicationContext());
		RealmConfiguration config = new RealmConfiguration.Builder()
				.schemaVersion(1) // Must be bumped when the schema changes
				.migration(new MyMigration()) // Migration to run
				.build();

		Realm.setDefaultConfiguration(config);

// This will automatically trigger the migration if needed
		Realm realm = Realm.getDefaultInstance();




		//UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
		//     LeakCanary.install(this);
		mInstance = this;

	}

	public static synchronized AppController getInstance() {
		return mInstance;
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(newBase);

		MultiDex.install(this);
		// super.attachBaseContext();
	}
}
