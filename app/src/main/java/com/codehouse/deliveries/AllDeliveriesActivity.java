package com.codehouse.deliveries;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.codehouse.deliveries.Adapters.DeliveryAdapter;
import com.codehouse.deliveries.Api.ApiEndpoint;
import com.codehouse.deliveries.Api.ApiUrl;
import com.codehouse.deliveries.DataModules.DeliveryModule;
import com.codehouse.deliveries.DataModules.LocationDeliveryModule;
import com.codehouse.deliveries.Misc.DeliveryObjects;
import com.codehouse.deliveries.Misc.EndlessScrollListener;
import com.codehouse.deliveries.RealmDatabase.Deliveries;
import com.codehouse.deliveries.RealmDatabase.DeliveryLocation;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class AllDeliveriesActivity extends AppCompatActivity {
    static int limit,offset;
    private RecyclerView recyclerView;
    static SwipeRefreshLayout mSwipeRefreshLayout;
    static public DeliveryAdapter adapter;
    public static List<DeliveryObjects> deliveryObjectsList;
    private EndlessScrollListener scrollListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        limit = 20;
        offset = 0;
        setContentView(R.layout.activity_all_deliveries);


        recyclerView = findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });



        deliveryObjectsList = new ArrayList<>();
        adapter = new DeliveryAdapter(AllDeliveriesActivity.this, deliveryObjectsList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(AllDeliveriesActivity.this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        scrollListener = new EndlessScrollListener() {
            @Override
            public void onLoadMore() {
                loadDeliveries();
            }

        };
        recyclerView.addOnScrollListener(scrollListener);
        fillRecyclerView();
        loadDeliveries();
    }

    void refreshItems() {
        //offset = 0;
        // Load items
        // ...
        loadDeliveries();
        // Load complete
    }

    void onItemsLoadComplete() {

        // Update the adapter and notify data set changed
        // ...
        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }



    void  loadDeliveries()
    {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.getBase())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiEndpoint endpoints = retrofit.create(ApiEndpoint.class);

        Call<List<DeliveryModule>> login = endpoints.retrieveDeliveries(offset);
        login.enqueue(new Callback<List<DeliveryModule>>() {
            @Override
            public void onResponse(Response<List<DeliveryModule>> response, Retrofit retrofit) {
                List<DeliveryModule> deliveryModuleList = response.body();
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                if(deliveryModuleList != null)
                for (int i = 0; i< deliveryModuleList.size(); i++)
                {
                    DeliveryModule deliveryModule = deliveryModuleList.get(i);
                    Deliveries delivery = realm.where(Deliveries.class).equalTo("deliveryId",deliveryModule.getId()).findFirst();
                    if(delivery == null)
                    {
                        Deliveries deliveries = new Deliveries();
                        deliveries.setId(realm.where(Deliveries.class).findAll().size() + 1);
                        deliveries.setDeliveryId(deliveryModule.getId());
                        deliveries.setDescription(deliveryModule.getDescription());
                        deliveries.setImageUrl(deliveryModule.getImageUrl());
                        LocationDeliveryModule locationDeliveryModule = deliveryModule.getLocation();
                        DeliveryLocation deliveryLocation = new DeliveryLocation();
                        deliveryLocation.setAddress(locationDeliveryModule.getAddress());
                        deliveryLocation.setDeliveryId(deliveryModule.getId());
                        deliveryLocation.setLat(locationDeliveryModule.getLat());
                        deliveryLocation.setLng(locationDeliveryModule.getLng());
                        deliveryLocation.setDeliveryLocationId(realm.where(DeliveryLocation.class).findAll().size() + 1);
                        realm.copyToRealmOrUpdate(deliveries);
                        realm.copyToRealmOrUpdate(deliveryLocation);
                    }
                }

                offset += limit;


                realm.commitTransaction();
                realm.close();
                fillRecyclerView();
                onItemsLoadComplete();
            }
            @Override
            public void onFailure(Throwable t) {
                fillRecyclerView();

                t.printStackTrace();
                Toast.makeText(AllDeliveriesActivity.this,"Sorry an error was encountered ",Toast.LENGTH_LONG).show();

            }
        });

    }

    private void fillRecyclerView()
    {
        deliveryObjectsList.clear();
        Realm realm= Realm.getDefaultInstance();
        RealmResults<Deliveries> deliveriesRealmResults = realm.where(Deliveries.class).findAll();

        for (int i = 0; i < deliveriesRealmResults.size(); i++)
        {
            //deliveryObjectsList
            DeliveryLocation deliveryLocation = realm.where(DeliveryLocation.class).equalTo("deliveryId",deliveriesRealmResults.get(i).getDeliveryId()).findFirst();
            DeliveryObjects deliveryObject = new DeliveryObjects();
            deliveryObject.setAddress(deliveryLocation.getAddress());
            deliveryObject.setDescription(deliveriesRealmResults.get(i).getDescription());
            deliveryObject.setId(deliveriesRealmResults.get(i).getId());
            deliveryObject.setImageUrl(deliveriesRealmResults.get(i).getImageUrl());
            deliveryObject.setLat(deliveryLocation.getLat());
            deliveryObject.setLng(deliveryLocation.getLng());
            deliveryObjectsList.add(deliveryObject);
        }
        adapter.notifyDataSetChanged();
    }


}
