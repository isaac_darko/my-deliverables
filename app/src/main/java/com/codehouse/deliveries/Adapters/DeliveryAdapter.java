package com.codehouse.deliveries.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.codehouse.deliveries.DeliveryDetailActivity;
import com.codehouse.deliveries.Misc.DeliveryObjects;
import com.codehouse.deliveries.R;
import com.codehouse.deliveries.ViewHolders.DeliveryViewHolder;

import java.util.List;

/**
 *
 *
 * Created by isaac on 9/17/18.
 *
 */

public class DeliveryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<DeliveryObjects> deliveryObjectsList;
    public static TextView title;
    public DeliveryAdapter(Context mContext, List<DeliveryObjects> deliveries) {
        this.mContext = mContext;
        this.deliveryObjectsList = deliveries;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater itemView = LayoutInflater.from(parent.getContext());
        View view = null;
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = itemView.inflate(R.layout.delivery_account_row, parent, false);
        viewHolder = new DeliveryViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        DeliveryViewHolder vh1 = (DeliveryViewHolder) holder;
        configureApplication(vh1, (DeliveryObjects) deliveryObjectsList.get(position), position);
    }

    void configureApplication(DeliveryViewHolder deliveryViewHolder, final DeliveryObjects deliveryObjectsList, int position)
    {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.logo)
                .error(R.drawable.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(mContext).load(deliveryObjectsList.getImageUrl())
                .thumbnail(0.5f)
                .apply(options)
                .into(deliveryViewHolder.getImageView());
        deliveryViewHolder.getDescription().setText(deliveryObjectsList.getDescription());
        deliveryViewHolder.getLocation().setText(deliveryObjectsList.getAddress());

        deliveryViewHolder.getLocation().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDeliveryDetail(deliveryObjectsList);
            }
        });
        deliveryViewHolder.getDescription().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDeliveryDetail(deliveryObjectsList);
            }
        });
        deliveryViewHolder.getImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToDeliveryDetail(deliveryObjectsList);
            }
        });

    }
    private void goToDeliveryDetail(DeliveryObjects deliveryObject)
    {
        Intent intent = new Intent(mContext, DeliveryDetailActivity.class);
        intent.putExtra("deliveryObject",deliveryObject);
        mContext.startActivity(intent);
    }
    @Override
    public int getItemCount() {
        return deliveryObjectsList.size();
    }

}
