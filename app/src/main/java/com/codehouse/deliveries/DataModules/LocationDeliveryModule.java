package com.codehouse.deliveries.DataModules;

import java.io.Serializable;

/**
 * Created by isaac on 9/17/18.
 */

public class LocationDeliveryModule {
    float lat,lng;
    String address;

    public LocationDeliveryModule() {
    }

    public LocationDeliveryModule(float lat, float lng, String address) {
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
