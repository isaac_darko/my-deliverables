package com.codehouse.deliveries.DataModules;

import java.io.Serializable;

/**
 * Created by isaac on 9/17/18.
 */

public class DeliveryModule {
    int id;
    String description,imageUrl;
    LocationDeliveryModule location;

    public DeliveryModule(int id, String description, String imageUrl, LocationDeliveryModule location) {
        this.id = id;
        this.description = description;
        this.imageUrl = imageUrl;
        this.location = location;
    }

    public DeliveryModule() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LocationDeliveryModule getLocation() {
        return location;
    }

    public void setLocation(LocationDeliveryModule location) {
        this.location = location;
    }
}
