package com.codehouse.deliveries.ViewHolders;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codehouse.deliveries.R;

/**
 * Created by isaac on 9/17/18.
 */

public class DeliveryViewHolder extends RecyclerViewEmptySupport.ViewHolder {
    TextView description,location;
    ImageView imageView;

    public DeliveryViewHolder(View itemView) {
        super(itemView);
        description = itemView.findViewById(R.id.deliveryDescription);
        location = itemView.findViewById(R.id.deliveryLocation);
        imageView = itemView.findViewById(R.id.mainImageView);
    }

    public DeliveryViewHolder(View itemView, TextView description, TextView location, ImageView imageView) {
        super(itemView);
        this.description = description;
        this.location = location;
        this.imageView = imageView;
    }

    public TextView getDescription() {
        return description;
    }

    public void setDescription(TextView description) {
        this.description = description;
    }

    public TextView getLocation() {
        return location;
    }

    public void setLocation(TextView location) {
        this.location = location;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }
}
