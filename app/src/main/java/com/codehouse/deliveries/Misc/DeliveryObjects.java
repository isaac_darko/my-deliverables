package com.codehouse.deliveries.Misc;

import com.codehouse.deliveries.DataModules.LocationDeliveryModule;

import java.io.Serializable;

/**
 * Created by isaac on 9/17/18.
 */

public class DeliveryObjects implements Serializable {
    int id;
    String description,imageUrl;
    float lat,lng;
    String address;

    public DeliveryObjects() {
    }

    public DeliveryObjects(int id, String description, String imageUrl, float lat, float lng, String address) {
        this.id = id;
        this.description = description;
        this.imageUrl = imageUrl;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
