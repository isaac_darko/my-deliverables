package com.codehouse.deliveries.Api;

import com.codehouse.deliveries.DataModules.DeliveryModule;
import com.codehouse.deliveries.DataModules.LocationDeliveryModule;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by isaac on 9/17/18.
 */

public interface ApiEndpoint {

    @GET("deliveries?limit=20")
    Call<List<DeliveryModule>> retrieveDeliveries(@Query("offset") int offset);

}
