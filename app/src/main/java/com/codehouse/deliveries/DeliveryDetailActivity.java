package com.codehouse.deliveries;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.codehouse.deliveries.Misc.DeliveryObjects;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DeliveryDetailActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    ImageView imageView;
    TextView textView;
    DeliveryObjects deliveryObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_detail);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        try{
          Intent intent1 = getIntent();
          deliveryObjects = (DeliveryObjects) intent1.getExtras().get("deliveryObject");
          if(deliveryObjects == null)
          {
              Intent intent = new Intent(DeliveryDetailActivity.this,AllDeliveriesActivity.class);
              startActivity(intent);
              finish();

          }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Intent intent = new Intent(DeliveryDetailActivity.this,AllDeliveriesActivity.class);
            startActivity(intent);
            finish();
        }
        imageView = findViewById(R.id.mainImageView);
        textView = findViewById(R.id.deliveryDescription);

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.logo)
                .error(R.drawable.logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(DeliveryDetailActivity.this).load(deliveryObjects.getImageUrl())
                .thumbnail(0.5f)
                .apply(options)
                .into(imageView);
        textView.setText(deliveryObjects.getDescription()+" At "+deliveryObjects.getAddress());
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng selectedDelivery = new LatLng(deliveryObjects.getLat(), deliveryObjects.getLng());
        mMap.addMarker(new MarkerOptions().position(selectedDelivery).title(deliveryObjects.getDescription()+" At "+deliveryObjects.getAddress()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(selectedDelivery));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(18);
        mMap.animateCamera(zoom);
    }
}
