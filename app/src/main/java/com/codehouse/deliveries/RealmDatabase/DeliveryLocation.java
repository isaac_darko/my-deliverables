package com.codehouse.deliveries.RealmDatabase;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by isaac on 9/17/18.
 */

public class DeliveryLocation extends RealmObject {
    @PrimaryKey
    int deliveryLocationId;

    int deliveryId;
    float lat,lng;
    String address;

    public DeliveryLocation() {
    }

    public DeliveryLocation(int deliveryLocationId, int deliveryId, float lat, float lng, String address) {
        this.deliveryLocationId = deliveryLocationId;
        this.deliveryId = deliveryId;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getDeliveryLocationId() {
        return deliveryLocationId;
    }

    public void setDeliveryLocationId(int deliveryLocationId) {
        this.deliveryLocationId = deliveryLocationId;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
