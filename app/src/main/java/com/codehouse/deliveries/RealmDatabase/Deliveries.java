package com.codehouse.deliveries.RealmDatabase;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by isaac on 9/17/18.
 */

public class Deliveries extends RealmObject{
    @PrimaryKey
    int deliveryId;

    int id;
    String description,imageUrl,location;

    public Deliveries() {
    }

    public Deliveries(int deliveryId, int id, String description, String imageUrl, String location) {
        this.deliveryId = deliveryId;
        this.id = id;
        this.description = description;
        this.imageUrl = imageUrl;
        this.location = location;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
